<?php declare(strict_types=1);

// Run local test server
// php -S localhost:8080 graphql.php

// Try query
// curl -d '{"query": "query { echo(message: \"Hello World\") }" }' -H "Content-Type: application/json" http://localhost:8080

// Try mutation
// curl -d '{"query": "mutation { sum(x: 2, y: 2) }" }' -H "Content-Type: application/json" http://localhost:8080
//https://compatt.medium.com/spatial-data-api-with-graphql-php-and-mysql-a48e8b89d4e1

/**
 * 
 * 
 * 
 query{
  totalPost
  post(id:172) {
    id,
    title,
    views,
    publish
  },
  posts{
    id,
		title,
    keyword
  }
}



mutation{
  updatePost(id:172,views:12,publish:1){
    title,
    views
    publish
  }
  createPost(title:"hoanguyenit",
    keyword:"hoa",des:"hoa",
    image:"hoa.jpg",content:"hoa it",
    user_id:2,views:100,publish:0,slug:"hoa-it"){
    title,
    views,
    publish
    id
  },
  deletePost(id:177)
}

 */

require_once __DIR__ . '/vendor/autoload.php';
use GraphQL\GraphQL;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;
use GraphQL\Type\Schema;
require "settings.ini.php";
require "Db.class.php";

try {
    //initialisation of database connection
	$db = new Db($host,$dbname,$user,$password);
    $user  = new ObjectType([
        'name'=>'user',
        'fields'=>[
            'id'=>['type'=>Type::int()],
            'name'=>['type'=>Type::string()],
            'email'=>['type'=>Type::string()],   
        ]
    ]);
    $post = new ObjectType([
        'name' => 'post',
        'fields' => [
            'id'=>['type'=>Type::int()],
            'title'=> ['type' => Type::string()],
            'keyword'=>['type'=>Type::string()],
            'des'=>['type'=>Type::string()],
            'slug'=>['type'=>Type::string()],
            'image'=>['type'=>Type::string()],
            'content'=>['type'=>Type::string()],
            'user_id'=>['type'=>Type::int()],
            'views'=>['type'=>Type::int()],
            'publish'=>['type'=>Type::int()],
            'user'=>$user,
        ]
    ]);
   
    $queryType = new ObjectType([
        'name' => 'Query',
        'fields' => [
            'totalPost' => [
                'type' => Type::int(),
                'resolve' => function($root,$args,$context){
                    global $db;
                    $result =$db->query('SELECT * FROM `posts`');
                    return count($result);
                }
            ],
            'posts' => [
                'type' => Type::listOf($post),
                'args'=>[
                    'title'=> ['type' => Type::string()],
                    'keyword'=>['type' => Type::string()],
                    'user'=>$user
                ],
                'resolve' => function ($root, $args, $context) {
                    global $db;
                    $results =$db->query("SELECT * FROM `posts` order by id desc");
                    foreach($results as $row) {
                           $user = $db->row("SELECT * FROM `users` where id=".$row['user_id']);
                            $rows[] =array(
                                'id'=>$row['id'],
                                'title'=>$row['title'],
                                'keyword'=>$row['keyword'],
                                'views'=>$row['views'],
                                'publish'=>$row['publish'],
                                'user'=>$user
                            );
                            
                        }
                    return $rows;
                }
            ],
            'post'=>[
                'type'=>$post,
                'args'=>[
                    'id' => ['type'=>Type::int()]
                ],
                'resolve' => function ($root, $args, $context) {
                    global $db;
                    $results = $db->row("SELECT * FROM `posts` where `id`=".$args['id']);
                    $user = $db->row("SELECT * FROM `users` where id=".$results['user_id']);
                    $results['user']= $user;
                    return $results;

                }
            ],
           
        ],
    ]);
    $mutationType = new ObjectType([
        'name' => 'Mutation',
        'fields' => [
            'sum' => [
                'type' => Type::int(),
                'args' => [
                    'x' => ['type' => Type::int()],
                    'y' => ['type' => Type::int()],
                ],
                'resolve' => static fn ($calc, array $args): int => $args['x'] + $args['y'],
            ],
            'updatePost'=>[
                'type' =>$post,
                'args' => [
                   'id'=>['type'=>Type::int()],
                   'views'=>['type'=>Type::int()],
                   'publish'=>['type'=>Type::int()]
                ],
                'resolve' => function($root,$args,$context){
                    global $db;
                    $sql = "UPDATE posts SET views=:views, publish=:publish WHERE id=:id";
                    $result = $db->query($sql,$args);
                    $results =$db->row("SELECT * FROM `posts` where `id`=".$args['id']);
                    return $results;
                }
            ],
            'createPost'=>[
                'type'=>$post,
                'args'=>[
                    'title'=> ['type' => Type::string()],
                    'keyword'=>['type'=>Type::string()],
                    'des'=>['type'=>Type::string()],
                    'slug'=>['type'=>Type::string()],
                    'image'=>['type'=>Type::string()],
                    'content'=>['type'=>Type::string()],
                    'user_id'=>['type'=>Type::int()],
                    'views'=>['type'=>Type::int()],
                    'publish'=>['type'=>Type::int()]
                ],
                'resolve' => function($root,$args,$context){

                    global $db;

                    $sql = "INSERT INTO posts(title,keyword,des,slug,image,content,user_id,views,publish)
                     VALUES(:title,:keyword,:des,:slug,:image,:content,:user_id,:views,:publish)";

                    $result = $db->query($sql,$args);

                    $lastInsertID = $db->lastInsertId();
                  
                    $results =$db->row("SELECT * FROM `posts` where `id`=".$lastInsertID);
                    
                    return $results;

                }
            ],
            'deletePost'=>[
                'type'=>Type::boolean(),
                'args'=>[
                    'id'=>Type::int()
                ],
                'resolve'=>function($root,$args,$context){
                    global $db;
                    $sql = "DELETE FROM `posts` WHERE `id`=:id";
                    $result = $db->query($sql,$args);
                    if($result) return true;
                    return false;

                }
            ]
        ],
    ]);



    // See docs on schema options:
    // https://webonyx.github.io/graphql-php/schema-definition/#configuration-options
    $schema = new Schema([
        'query' => $queryType,
        'mutation' => $mutationType,
    ]);

    $rawInput = file_get_contents('php://input');
    if ($rawInput === false) {
        throw new RuntimeException('Failed to get php://input');
    }

    $input = json_decode($rawInput, true);
    $query = $input['query'];
    $variableValues = $input['variables'] ?? null;

    $rootValue = ['prefix' => 'You said: '];
    $result = GraphQL::executeQuery($schema, $query, $rootValue, null, $variableValues);
    $output = $result->toArray();
} catch (Throwable $e) {
    $output = [
        'error' => [
            'message' => $e->getMessage(),
        ],
    ];
}

header('Content-Type: application/json; charset=UTF-8');
echo json_encode($output);