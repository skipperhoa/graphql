<?php

class DB
{
    private static $pdo;
    
    public static function init($config)
    {
        self::$pdo = new PDO("mysql:host={$config['host']};dbname={$config['database']}", $config['username'], $config['password']);
        self::$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    public static function select($query)
    {
        $statement = self::$pdo->query($query);
        return $statement->fetchAll();
    }
    public static function execute($query,$data){
        $statement =  self::$pdo->prepare($query);
        if($statement->execute($data)){
            return 1;
        }
        return 0;
    }
}